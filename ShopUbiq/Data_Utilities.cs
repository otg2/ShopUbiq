﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Telerik.Web.UI;

/// <summary>
/// Summary description for Data_Utilities
/// </summary>
public static class Data_Utilities
{
    // Same as above, but works for SQL databases
    public static DataTable getSQLDataByQuery(string aQuery)
    {
        DataTable _dataView = new DataTable();
        ConnectionStringSettings _connString = ConfigurationManager.ConnectionStrings["ConnectionString"];
        using (SqlConnection con = new SqlConnection(_connString.ToString()))
        {
            con.Open();
            SqlCommand _command = new SqlCommand();
            _command.Connection = con;
            _command.CommandText = aQuery;
            _command.ExecuteNonQuery();

            SqlDataAdapter sda = new SqlDataAdapter(_command);
            sda.Fill(_dataView);
            con.Close();
        }
        return _dataView;
    }

    // Same as above, but works for SQL databases
    public static DataTable getSQLDataByQuery_Parameters(string aQuery, List<String[]> aParameters)
    {
        DataTable _dataView = new DataTable();
        ConnectionStringSettings _connString = ConfigurationManager.ConnectionStrings["ConnectionString"];
        using (SqlConnection con = new SqlConnection(_connString.ToString()))
        {
            con.Open();
            SqlCommand _command = new SqlCommand();
            _command.Connection = con;
            _command.CommandText = aQuery;
            for (int i = 0; i < aParameters.Count; i++) _command.Parameters.AddWithValue(aParameters[i][0], aParameters[i][1]);

            SqlDataAdapter sda = new SqlDataAdapter(_command);
            sda.Fill(_dataView);
            con.Close();
        }
        return _dataView;
    }

    public static void ModifyDataBase_Parameters(string aQuery, List<String[]> aParameters)
    {
        ConnectionStringSettings _connString = ConfigurationManager.ConnectionStrings["ConnectionString"];
        using (SqlConnection con = new SqlConnection(_connString.ToString()))
        {
            con.Open();
            SqlCommand _command = new SqlCommand();
            _command.Connection = con;

            // Set values
            for (int i = 0; i < aParameters.Count; i++) _command.Parameters.AddWithValue(aParameters[i][0], aParameters[i][1]);

            _command.CommandText = aQuery;
            _command.ExecuteNonQuery();


        }
    }

   
}