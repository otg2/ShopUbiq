﻿
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Device.Location;
using System.Globalization;

namespace ShopUbiq
{
    public partial class _Default : Page
    {

        private RadButton[] _buttonArray = new RadButton[10];

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            if(!IsPostBack)
            {
                
            }

            _buttonArray[0] = Item_Apple;
            _buttonArray[1] = Item_Avacado;
            _buttonArray[2] = Item_Banana;
            _buttonArray[3] = Item_Kiwi;
            _buttonArray[4] = Item_Lemon;
            _buttonArray[5] = Item_Lime;
            _buttonArray[6] = Item_Orange;
            _buttonArray[7] = Item_Pear;
            _buttonArray[8] = Item_Pineapple;
            _buttonArray[9] = Item_Strawberry;
        }

        protected void FindItems_Button_Click(object sender, EventArgs e)
        {
            
        }

        protected void Veggies_AjaxManager_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            string[] _commandString = e.Argument.Split('|');

            bool onlyAllItems = false;

            if (_commandString[0] == "findAvailStore")
            {
                onlyAllItems = true;
                _commandString[0] = "findVeggies";
            }

            if(_commandString[0] == "findVeggies")
            {
                string[] cords = _commandString[1].Split(';');

                double _userLat = Convert.ToDouble(cords[0]);
                double _userLong = Convert.ToDouble(cords[1]);

                debug.Text += "MYCORDS_" + _userLat.ToString() + ";" + _userLong.ToString();

                var closestStore = GetClosestStore(_userLat, _userLong, onlyAllItems);
                var closestStoreId = Convert.ToInt32(closestStore[0]);

                MissingItems_Button.Value = closestStore[0].ToString();

                // Get selection
                string _itemSelectionString = "SELECT [RACK_ID], [NAME], [PRICE], [STATUS] " +
                                                "FROM [shopprojectdata].[dbo].[ITEMS] " +
                                                "WHERE [NAME] in (";

                string _upsellCheck = "";
                string _upsellAddInfo = "[PROD_NAME]";
                ItemSelection.Items.Clear();
                int _selectedItems = 0;
                for(int i = 0; i < _buttonArray.Length; i++)
                {
                    bool _wasSelected = _buttonArray[i].Checked;
                    if(_wasSelected)
                    {
                        _selectedItems++;
                        string _itemString = _buttonArray[i].ID.ToString().Split('_')[1];
                        _itemSelectionString += "'" + _itemString.ToUpper() + "',";

                        _upsellCheck += "[PROD_NAME] like '%" + _itemString.ToUpper() + "%' AND ";

                        _upsellAddInfo = "REPLACE(" + _upsellAddInfo + ",'" + _itemString.ToUpper() + @"','')";

                        ItemSelection.Items.Insert(0, new DropDownListItem(_itemString, _itemString));                        
                    }
                    
                }
                ItemSelection.Items.Sort();
                ItemSelection.Items.Insert(0, new DropDownListItem("Select item"));
                ItemSelection.DataBind();
                

                //debug.Text = _itemSelectionString;

                if (_selectedItems != 0)
                {
                    ItemSelection.Visible = true;
                    MissingItems_Button.Visible = true;
                    // GET THE RACKS
                    string _getRackQuery = "SELECT [NODE_ID], [X], [Y], [WIDTH], [HEIGHT] " +
                                            "From [shopprojectdata].[dbo].[RACKS] " +
                                            "Where [STORE_ID]=" + closestStoreId.ToString();

                    DataTable _rackInfo = Data_Utilities.getSQLDataByQuery(_getRackQuery);

                    // Creates a string that the client uses to turn into an object
                    string _rackString = "";
                    int searchedItems_Racks = _rackInfo.Rows.Count;

                    for (int i = 0; i < searchedItems_Racks; i++)
                    {
                        _rackString += _rackInfo.Rows[i][0].ToString() + ";";                 // NODE_ID
                        _rackString += _rackInfo.Rows[i][1].ToString() + ";";                 // X
                        _rackString += _rackInfo.Rows[i][2].ToString() + ";";                 // Y
                        _rackString += _rackInfo.Rows[i][3].ToString() + ";";                 // WIDTH
                        _rackString += _rackInfo.Rows[i][4].ToString();                       // HEIGHT

                        // Add splitter at end if last item
                        if (i < searchedItems_Racks - 1)
                        {
                            _rackString += "_";
                        }
                    }

                    RackHolder.Text = _rackString;

                    // GET THE NODES
                    string _getNodeQuery = "SELECT [X], [Y], [ADJACENCIES] from [shopprojectdata].[dbo].[NODES] where [STORE_ID]=" + closestStoreId;
                    DataTable _nodeInfo = Data_Utilities.getSQLDataByQuery(_getNodeQuery);

                    // Creates a string that the client uses to turn into an object
                    string _nodeString = "";
                    int searchedItems_Nodes = _nodeInfo.Rows.Count;

                    for (int i = 0; i < searchedItems_Nodes; i++)
                    {
                        _nodeString += _nodeInfo.Rows[i][0].ToString() + ";";                 // RACK_ID
                        _nodeString += _nodeInfo.Rows[i][1].ToString() + ";";                 // NAME
                        _nodeString += _nodeInfo.Rows[i][2].ToString();                       // PRICE

                        // Add splitter at end if last item
                        if (i < searchedItems_Nodes - 1)
                        {
                            _nodeString += "_";
                        }
                    }
                    NodeHolder.Text = _nodeString;

                    // Check for upsell here
                    DateTime _today = DateTime.Now;

                    string _upsellQuery = "SELECT " + _upsellAddInfo + " as AddInfo, UPSELL_PRICE from (select * from [shopprojectdata].[dbo].[UPSELL] where [ACTIVE] = 1 AND ";
                    _upsellQuery += "(SELECT '" + _today.ToString("yyyy-MM-dd")+ "' ) ";
                    _upsellQuery += "BETWEEN [DATE_FROM] AND [DATE_TO] AND ";
                    _upsellQuery += "(select cast('" + _today.ToString("HH:mm:ss")+ "' as time)) ";
                    _upsellQuery += "BETWEEN [TIME_FROM] AND [TIME_TO] ";
                    _upsellQuery += "AND ";
                    _upsellQuery += _upsellCheck;

                    // remove last AND
                    _upsellQuery = _upsellQuery.Substring(0, _upsellQuery.Length - 4);
                    _upsellQuery += " ) editQuery ORDER BY UPSELL_PRICE DESC";

                    debug.Text = "";// _upsellQuery;

                    DataRow _selectedUpsell = null;
                    string[] _addToUpsell = null;
                    
                    if (_selectedItems >= 3)
                    {
                        DataTable _upsellInfo = Data_Utilities.getSQLDataByQuery(_upsellQuery);
                        // Just use the first row..
                        if (_upsellInfo.Rows.Count > 0)
                        {
                            _selectedUpsell = _upsellInfo.Rows[0];
                        }
                    }

                    if (_selectedUpsell != null)
                    {
                        _addToUpsell = _selectedUpsell[0].ToString().Split('_');
                        for(int i = 0 ; i < _addToUpsell.Length;i++)
                        {
                            string _upsell = _addToUpsell[i].Trim();
                            if(!String.IsNullOrEmpty(_upsell))
                            {
                                _itemSelectionString += "'" + _upsell + "',";
                            }
                        }
                    }

                    // remove last comma
                    _itemSelectionString = _itemSelectionString.Substring(0, _itemSelectionString.Length - 1);

                    _itemSelectionString += ") and [STORE_ID]=" + closestStoreId.ToString();
                    // GET THE ITEMS
                    DataTable _itemInfo = Data_Utilities.getSQLDataByQuery(_itemSelectionString);

                    // Creates a string that the client uses to turn into an object
                    string _dataString = "";
                    string _warningString = "";
                    int searchedItems = _itemInfo.Rows.Count;

                    for (int i = 0; i < searchedItems; i++)
                    {
                        string _shouldDraw = "1";
                        if (_addToUpsell != null)
                            if (_addToUpsell.Contains(_itemInfo.Rows[i][1].ToString())) _shouldDraw = "0";


                        _dataString += _itemInfo.Rows[i][0].ToString() + ";";                 // RACK_ID
                        _dataString += _itemInfo.Rows[i][1].ToString() + ";";                 // NAME
                        _dataString += _itemInfo.Rows[i][2].ToString() + ";";                 // PRICE
                        _dataString += _shouldDraw;                                             // DRAW

                        if (_itemInfo.Rows[i][3].ToString() == "UNAVAIL" && _shouldDraw == "1")
                        {
                            _warningString += "Warning - we are out of " + _itemInfo.Rows[i][1].ToString() + "<br/>";
                        }
                        if (_itemInfo.Rows[i][3].ToString() == "PENDING" && _shouldDraw == "1")
                        {
                            _warningString += "Warning - someone marked " + _itemInfo.Rows[i][1].ToString() + " missing so maybe it's not available" + "<br/>";
                        }

                        // Add splitter at end if last item
                        if (i < searchedItems - 1)
                        {
                            _dataString += "_";
                        }
                    }

                    LabelWarning.InnerHtml = _warningString;
                    CoordinatesHolder.Text = _dataString;

                    if(!String.IsNullOrEmpty(_warningString))
                    {
                        ClosestAvailStore.Visible = true;
                    }
                    else
                    {
                        ClosestAvailStore.Visible = false;
                    }
                    // Set the store information visible

                    // _closestStore -1 cause index of stores in data is 1,2,3 while array index is 0,1,2...
                    TestLabel.Text = "Your closest store: " + closestStore[3].ToString();

                    // Init the javascript that draws the store
                    Veggies_AjaxManager.ResponseScripts.Add("initDrawStore();");

                }
                else
                {
                    TestLabel.Text = "Please select the products you are looking for";
                    ItemSelection.Visible = false;
                    MissingItems_Button.Visible = false;
                }
            }
        }

        private DataRow GetClosestStore(double latitude, double longitude,bool containsAll)
        {
            string    getStoreQuery = "SELECT [ID], [LAT], [LONG], [NAME] from [shopprojectdata].[dbo].[STORE]";
            if (containsAll)
            {
                getStoreQuery = "SELECT [ID], [LAT], [LONG], [NAME] from [shopprojectdata].[dbo].[STORE] where id not in (select [STORE_ID] from [shopprojectdata].[dbo].[ITEMS]  where status not in('AVAIL'))";
            }

            DataTable storeInfo = Data_Utilities.getSQLDataByQuery(getStoreQuery);


            var sLat = Convert.ToDouble(storeInfo.Rows[0][1].ToString());
            var sLong = Convert.ToDouble(storeInfo.Rows[0][2].ToString());

            //debug.Text += "CORDS - " + latitude.ToString() + "_" + longitude.ToString();

            var sCoord = new GeoCoordinate(sLat, sLong);
            var eCoord = new GeoCoordinate(latitude, longitude);

            var minDistance = eCoord.GetDistanceTo(sCoord);
            var store = storeInfo.Rows[0];

            //debug.Text += "_dist is " + minDistance.ToString() + " for " + storeInfo.Rows[0][3].ToString(); 

            for (int i = 1; i < storeInfo.Rows.Count; i++)
            {
                sLat = Convert.ToDouble(storeInfo.Rows[i][1].ToString());
                sLong = Convert.ToDouble(storeInfo.Rows[i][2].ToString());

                sCoord = new GeoCoordinate(sLat, sLong);
                var distance = eCoord.GetDistanceTo(sCoord);
                //debug.Text += "_dist is " + distance.ToString() + " for " + storeInfo.Rows[i][3].ToString(); 

                if (distance < minDistance)
                {
                    minDistance = distance;
                    store = storeInfo.Rows[i];
                }
            }
            return store;
        }

        private double GeoDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }
        
        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }
        
        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        protected void MissingItems_Button_Click(object sender, EventArgs e)
        {
            string _closestStore = ((RadButton)sender).Value;
            string _updateCommand = "update [shopprojectdata].[dbo].[ITEMS] set [STATUS]=@status where [STORE_ID]=@store_id and [NAME]=@name";

            List<string[]> _parameters = new List<string[]>()
                    {
                        new string[] { "@status", "PENDING"},
                        new string[] { "@store_id", _closestStore },
                        new string[] { "@name", ItemSelection.SelectedText.ToUpper() }

                    };
            Data_Utilities.ModifyDataBase_Parameters(_updateCommand, _parameters);

            NotificationLabel.Text = "Thanks for notifying us!";
        }

    }
}