﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web.UI;
using System.Device.Location;
//using TensorFlow;
//using TensorFlow;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Text;
using System.Threading;
using System.Globalization;

namespace ShopUbiq
{
    /*
        0: 'Apple Red , 
        1: 'Avocado', 
        2: 'Banana', 
        3: 'Kiwi', 
        4: 'Lemon', 
        5: 'Limes', 
        6: 'Orange', 
        7: 'Pear', 
        8: 'Pineapple', 
        9: 'Strawberry'
     */

    public partial class About : Page
    {

        // Use the tensorflow solution
        private static bool _usingTensorflow = true;
        private RadButton[] _buttonArray = new RadButton[10];

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            if(!IsPostBack)
            {
                Combo_Manage.Rebind();

                // Hide mobile section if not found
                if (!_usingTensorflow)
                {
                    USE_TENSOR_DIV.Style.Add("display", "none");
                }
            }
            _buttonArray[0] = Item_Apple;
            _buttonArray[1] = Item_Avacado;
            _buttonArray[2] = Item_Banana;
            _buttonArray[3] = Item_Kiwi;
            _buttonArray[4] = Item_Lemon;
            _buttonArray[5] = Item_Lime;
            _buttonArray[6] = Item_Orange;
            _buttonArray[7] = Item_Pear;
            _buttonArray[8] = Item_Pineapple;
            _buttonArray[9] = Item_Strawberry;
        }

        private void bindMissingWindow()
        {
            DataTable _pendingItems = Data_Utilities.getSQLDataByQuery("select name from [shopprojectdata].[dbo].[ITEMS] where status='PENDING' and store_id=" + SelectedStore_Id.Text);
            if (_pendingItems.Rows.Count > 0)
            {
                ReportedMissing_Button.Visible = true;
                for (int i = 0; i < _pendingItems.Rows.Count; i++)
                {
                    string _missingValue = _pendingItems.Rows[i][0].ToString();

                    HtmlGenericControl _divHolder = new HtmlGenericControl("div");
                    _divHolder.Style.Add("text-align", "center");

                    HtmlGenericControl _imgHolder = new HtmlGenericControl("div");

                    RadButton _img = new RadButton();
                    _img.AutoPostBack = false;
                    _img.ButtonType = RadButtonType.ToggleButton;
                    _img.Checked = false;
                    _img.Width = 200;
                    _img.Height = 200;
                    _img.Image.ImageUrl = "images/icons/" + _missingValue.ToLower() + ".png";

                    _imgHolder.Controls.Add(_img);

                    RadButton _confirmButton = new RadButton();
                    _confirmButton.Skin = "Telerik";
                    _confirmButton.Text = "Confirm missing";
                    _confirmButton.Icon.SecondaryIconCssClass = "rbOk";
                    _confirmButton.Icon.SecondaryIconRight = 5;
                    _confirmButton.AutoPostBack = false;
                    _confirmButton.RenderMode = RenderMode.Lightweight;
                    _confirmButton.OnClientClicked = "confirmMissingItem";
                    _confirmButton.CommandArgument = _missingValue;

                    RadButton _denyButton = new RadButton();
                    _denyButton.Skin = "Sunset";
                    _denyButton.Text = "Deny missing";
                    _denyButton.Icon.SecondaryIconCssClass = "rbCancel";
                    _denyButton.Icon.SecondaryIconRight = 5;

                    _denyButton.AutoPostBack = false;
                    _denyButton.RenderMode = RenderMode.Lightweight;
                    _denyButton.OnClientClicked = "denyMissingItem";
                    _denyButton.CommandArgument = _missingValue;

                    _divHolder.Controls.Add(_imgHolder);
                    _divHolder.Controls.Add(_confirmButton);
                    _divHolder.Controls.Add(_denyButton);

                    Display_MissingItems.Controls.Add(_divHolder);
                }

            }
            else
            {
                HtmlGenericControl _divHolder = new HtmlGenericControl("div");
                _divHolder.InnerHtml = "No missing items registered";
                Display_MissingItems.Controls.Add(_divHolder);
                //ReportedMissing_Button.Visible = false;
            }
        }

        protected void UpdateItem_Button_Click(object sender, EventArgs e)
        {
           //TODO: If nothing is selected, send a prompt message
            if(SelectUpdateItem.SelectedIndex == 0)
            {
                NotifyLabel.Text = "Please select an item you would like to update";
                return;
            }
            if (String.IsNullOrEmpty(Selected_Rack.Text.Trim()))
            {
                NotifyLabel.Text = "Please select a rack where you would like to place the product";
                return;
            }

            string _selectedClass = SelectUpdateItem.SelectedItem.Text.ToUpper();

            string _updateCmd = "update [shopprojectdata].[dbo].[ITEMS] set rack_id=@rack_id, status='AVAIL' where store_id=@store_id and name=@name";

            List<string[]> _parameters = new List<string[]>()
                    {
                        new string[] { "@rack_id", Selected_Rack.Text.Trim() },
                        new string[] { "@name", _selectedClass },
                        new string[] { "@store_id", SelectedStore_Id.Text.Trim()}

                    };
            
            Data_Utilities.ModifyDataBase_Parameters(_updateCmd, _parameters);

            NotifyLabel.Text = SelectUpdateItem.SelectedItem.Text + " location successfully updated in " + SelectedStore_Name.Text;
            //NotifyLabel.Text += Selected_Rack.Text.Trim() + "_" + _selectedClass + "_" + _selectedStore.ToString() + "_" + _updateCmd;
            StringBuilder cstext = new StringBuilder();
            cstext.Append("storeUpdate('");
            cstext.Append(_selectedClass + "')");
            ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", cstext.ToString(), true);
        }

        protected void Store_AjaxManager_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            string[] _commandString = e.Argument.Split('|');

            if(_commandString[0] == "firstInit")
            {
                string[] _coords = _commandString[1].Split(';');
                //debug.Text = "cords are - lat: " + _coords[0] + " long: " + _coords[1];
                // TODO: Find closest store and get name
                // TODO: Find a way to get coordinates here before using page load?
                //var eLat = 59.399052;
                //var eLong = 17.947836;
                var closestStore = GetClosestStore(Convert.ToDouble(_coords[0]), Convert.ToDouble(_coords[1]));
                //var closestStore = GetClosestStore(eLat, eLong);
                SelectedStore_Id.Text = closestStore[0].ToString();
                SelectedStore_Name.Text = closestStore[3].ToString();
                DisplayStore.InnerText += SelectedStore_Name.Text + " location";

                // Only display button if item is marked 
                bindMissingWindow();

                // Draw the store in the client
                drawStoreInClient(SelectedStore_Id.Text);

                SelectUpdateItem.DataSource = Data_Utilities.getSQLDataByQuery("select distinct name from [shopprojectdata].[dbo].[ITEMS] where store_id=" + SelectedStore_Id.Text);
                SelectUpdateItem.DataBind();
                SelectUpdateItem.Items.Insert(0, new DropDownListItem("Select an item"));

            }

            if (_commandString[0] == "confirmMiss")
            {
                string _updateCmd = "update [shopprojectdata].[dbo].[ITEMS] set status='UNAVAIL' where store_id=@store_id and name=@name";
                List<string[]> _parameters = new List<string[]>()
                    {
                        new string[] { "@name", _commandString[1] },
                        new string[] { "@store_id", SelectedStore_Id.Text.Trim()}
                    };
                Data_Utilities.ModifyDataBase_Parameters(_updateCmd, _parameters);
                bindMissingWindow();
            }
            else if (_commandString[0] == "denyMiss")
            {
                string _updateCmd = "update [shopprojectdata].[dbo].[ITEMS] set status='AVAIL' where store_id=@store_id and name=@name";
                List<string[]> _parameters = new List<string[]>()
                    {
                        new string[] { "@name", _commandString[1] },
                        new string[] { "@store_id", SelectedStore_Id.Text.Trim()}
                    };
                
                Data_Utilities.ModifyDataBase_Parameters(_updateCmd, _parameters);
                bindMissingWindow();
            }
            else if(_commandString[0] == "ClassifyImage")
            {
                Random _rnd = new Random();
                int randName = _rnd.Next(1, 10000); // creates a number between 1 and 12

                System.Drawing.Image img = Base64ToImage(_commandString[1]);
                //img.Save(Server.MapPath("~/UBIQ_TENSOR/IMG_TO_CLASSIFY.jpg"));

                Bitmap bmp = ResizeImage(img, 32, 32);
                bmp.Save(Server.MapPath("~/UBIQ_TENSOR/IMG_TO_CLASSIFY.jpg"));
                //bmp.Save(Server.MapPath("~/Images/mobileMin" + DateTime.Now.ToString("dd.MM.yyyy") + "_" + randName.ToString() + ".jpg"));
                
                //imgToRGB(img, randName);
                
                WebClient web = new WebClient();
                System.IO.Stream stream = web.OpenRead("http://127.0.0.1:5000/");
                using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
                {
                    // Replies are in [X]
                    string text = reader.ReadToEnd();
                    string _index = text.Substring(1, 1);
                    SelectUpdateItem.SelectedIndex = Convert.ToInt32(_index) + 1; // The first one is "select an item"
                }
            }
             
        }

        private DataRow GetClosestStore(double latitude, double longitude)
        {

            string getStoreQuery = "SELECT [ID], [LAT], [LONG], [NAME] from [shopprojectdata].[dbo].[STORE]";
            DataTable storeInfo = Data_Utilities.getSQLDataByQuery(getStoreQuery);


            var sLat = Convert.ToDouble(storeInfo.Rows[0][1].ToString());
            var sLong = Convert.ToDouble(storeInfo.Rows[0][2].ToString());
            var sCoord = new GeoCoordinate(sLat, sLong);
            var eCoord = new GeoCoordinate(latitude, longitude);

            var minDistance = eCoord.GetDistanceTo(sCoord);
            var store = storeInfo.Rows[0];

            for (int i = 1; i < storeInfo.Rows.Count; i++)
            {
                sLat = Convert.ToDouble(storeInfo.Rows[i][1].ToString());                 
                sLong = Convert.ToDouble(storeInfo.Rows[i][2].ToString());                 
                sCoord = new GeoCoordinate(sLat, sLong);
                var distance = eCoord.GetDistanceTo(sCoord);
                if (distance < minDistance) {
                    minDistance = distance;
                    store = storeInfo.Rows[i];
                }
            }
            return store;
        }

        public Image Base64ToImage(string aBase64String)
        {
            string _splitDataString = aBase64String.Split(',')[1].Trim();

            byte[] _imageBytes = Convert.FromBase64String(_splitDataString);
            MemoryStream ms = new MemoryStream(_imageBytes, 0, _imageBytes.Length);
            ms.Write(_imageBytes, 0, _imageBytes.Length);
            Image _image = Image.FromStream(ms, true);

            Image _cropImage = ThumbnailImage(_image, 500, false, false, Color.White);


            return _cropImage;
        }

        public static Image ThumbnailImage(Image sourceImage, int imageSize, bool maintainAspectRatio, bool maintainImageSize, Color backgroundColor)
        {
                int thumbnailWidth = imageSize;
                int thumbnailHeight = imageSize;

                if (maintainAspectRatio)
                {
                    float aspectRatio = (float)sourceImage.Width / sourceImage.Height;
                    float targetAspectRatio = (float)thumbnailWidth / thumbnailHeight;

                    if (aspectRatio < targetAspectRatio)
                    {
                        thumbnailWidth = (int)(thumbnailHeight * aspectRatio);
                    }
                    else if (aspectRatio > targetAspectRatio)
                    {
                        thumbnailHeight = (int)(thumbnailWidth / aspectRatio);
                    }
                }

                System.Drawing.Image thumbnail = sourceImage.GetThumbnailImage(thumbnailWidth, thumbnailHeight, null, new IntPtr());

                if (maintainImageSize)
                {
                    var offset = new Point(0, 0);
                    if (thumbnailWidth != imageSize)
                    {
                        offset.X = ((imageSize - thumbnailWidth) / 2);
                    }
                    if (thumbnailHeight != imageSize)
                    {
                        offset.Y = ((imageSize - thumbnailHeight) / 2);
                    }

                    var bmpImage = new Bitmap(imageSize, imageSize, PixelFormat.Format32bppArgb);

                    using (Graphics graphics = Graphics.FromImage(bmpImage))
                    {
                        graphics.Clear(backgroundColor);
                        graphics.DrawImage(thumbnail, new Rectangle(offset.X, offset.Y, thumbnailWidth, thumbnailHeight), new Rectangle(0, 0, thumbnailWidth, thumbnailHeight), GraphicsUnit.Pixel);
                    }
                    thumbnail.Dispose();
                    return System.Drawing.Image.FromHbitmap(bmpImage.GetHbitmap());
                }
                return thumbnail;
            

        }

        private void imgToRGB(System.Drawing.Image anImgToConvert, int aRandName)
        {
            Bitmap bmp = ResizeImage(anImgToConvert, 32, 32);
            bmp.Save(Server.MapPath("~/Images/mobileMin" + DateTime.Now.ToString("dd.MM.yyyy") + "_" + aRandName.ToString() + ".jpg"));

            /*
            int _width = bmp.Width;
            int _height = bmp.Height;

            double[,] rArr = new double[_width, _height];
            double[,] gArr = new double[_width, _height];
            double[,] bArr = new double[_width, _height];

            using (bmp)
            {
                // TODO: Find a quicker way of normalizing
                for (int i = 0; i < _width; i++)
                {
                    for (int j = 0; j < _height; j++)
                    {
                        Color clr = bmp.GetPixel(i, j);
                        rArr[i, j] = Convert.ToDouble(clr.R / 255f) - 0.5f;
                        gArr[i, j] = Convert.ToDouble(clr.G / 255f) - 0.5f;
                        bArr[i, j] = Convert.ToDouble(clr.B / 255f) - 0.5f;
                    }
                }
                debug.Text = rArr[0, 0].ToString() + " _ " + gArr[0, 0].ToString() + " _ " + bArr[0, 0].ToString() + " ____ " + rArr[16, 16].ToString() + " _ " + gArr[16, 16].ToString() + " _ " + bArr[16, 16].ToString();
            }
            */
        }

        public Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            // TODO: Make rectangle and cut most of sides

            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }

        private double GeoDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }



       private void drawStoreInClient(string aSelectedStore)
        {
           
            // Get selection
            string _itemSelectionString = "SELECT [RACK_ID], [NAME], [PRICE] from [shopprojectdata].[dbo].[ITEMS] where [STORE_ID]=" + aSelectedStore;

            // GET THE RACKS
            string _getRackQuery = "SELECT [NODE_ID], [X], [Y], [WIDTH], [HEIGHT] from [shopprojectdata].[dbo].[RACKS] where [STORE_ID]=" + aSelectedStore;

            DataTable _rackInfo = Data_Utilities.getSQLDataByQuery(_getRackQuery);

            // Creates a string that the client uses to turn into an object
            string _rackString = "";
            int searchedItems_Racks = _rackInfo.Rows.Count;

            for (int i = 0; i < searchedItems_Racks; i++)
            {
                _rackString += _rackInfo.Rows[i][0].ToString() + ";";                 // NODE_ID
                _rackString += _rackInfo.Rows[i][1].ToString() + ";";                 // X
                _rackString += _rackInfo.Rows[i][2].ToString() + ";";                 // Y
                _rackString += _rackInfo.Rows[i][3].ToString() + ";";                 // WIDTH
                _rackString += _rackInfo.Rows[i][4].ToString();                       // HEIGHT

                // Add splitter at end if last item
                if (i < searchedItems_Racks - 1)
                {
                    _rackString += "_";
                }
            }

            RackHolder.Text = _rackString;

            // GET THE NODES
            string _getNodeQuery = "SELECT [X], [Y], [ADJACENCIES] from [shopprojectdata].[dbo].[NODES] where [STORE_ID]=" + aSelectedStore.ToString();
            DataTable _nodeInfo = Data_Utilities.getSQLDataByQuery(_getNodeQuery);

            // Creates a string that the client uses to turn into an object
            string _nodeString = "";
            int searchedItems_Nodes = _nodeInfo.Rows.Count;

            for (int i = 0; i < searchedItems_Nodes; i++)
            {
                _nodeString += _nodeInfo.Rows[i][0].ToString() + ";";                 // RACK_ID
                _nodeString += _nodeInfo.Rows[i][1].ToString() + ";";                 // NAME
                _nodeString += _nodeInfo.Rows[i][2].ToString();                       // PRICE

                // Add splitter at end if last item
                if (i < searchedItems_Nodes - 1)
                {
                    _nodeString += "_";
                }
            }
            NodeHolder.Text = _nodeString;

            // GET THE ITEMS
            DataTable _itemInfo = Data_Utilities.getSQLDataByQuery(_itemSelectionString);

            // Creates a string that the client uses to turn into an object
            string _dataString = "";
            int searchedItems = _itemInfo.Rows.Count;

            for (int i = 0; i < searchedItems; i++)
            {
                _dataString += _itemInfo.Rows[i][0].ToString() + ";";                 // RACK_ID
                _dataString += _itemInfo.Rows[i][1].ToString() + ";";                 // NAME
                _dataString += _itemInfo.Rows[i][2].ToString();                       // PRICE

                // Add splitter at end if last item
                if (i < searchedItems - 1)
                {
                    _dataString += "_";
                }
            }
            CoordinatesHolder.Text = _dataString;

            Store_AjaxManager.ResponseScripts.Add("initDrawStore();");

        }

       

       protected void Combo_CreateButton_Click(object sender, EventArgs e)
       {
           Random _rnd = new Random();
           int randName = _rnd.Next(1, 10000); // creates a number between 1 and 12

           string _insertCommand = "insert into [shopprojectdata].[dbo].[UPSELL] ([COMBO_ID], [PROD_NAME], [DATE_FROM], [DATE_TO], [TIME_FROM],[TIME_TO],[ACTIVE], [UPSELL_PRICE]) values (" ; //@combo_id,@prod_name,@date_to, @date_from, @time_to,@time_from,@active)";

           string _warningLabel = "";
           // TODO: If nothing is selected, cancel selection
           string _comboString = "";

           int _selectedItems = 0;

           double upsellAmount = 0;

           for (int i = 0; i < _buttonArray.Length; i++)
           {
               bool _wasSelected = _buttonArray[i].Checked;
               if (_wasSelected)
               {
                   _selectedItems++;
                   _comboString += _buttonArray[i].ID.ToString().Split('_')[1].ToUpper() + "_";
                   upsellAmount += Convert.ToDouble(Data_Utilities.getSQLDataByQuery("select [PRICE] FROM [shopprojectdata].[dbo].[ITEMS] where name = '" + _buttonArray[i].ID.ToString().Split('_')[1].ToUpper() + "'").Rows[0][0].ToString());
               }
           }

           if (Combo_DateFrom.DbSelectedDate == null || Combo_DateTo.DbSelectedDate == null ||
              Combo_TimeFrom.DbSelectedDate == null || Combo_TimeTo.DbSelectedDate == null)
           {
               _warningLabel = "Date selection not finished";
               ComboLabel.Text = _warningLabel;
               return;
           }

           // Must select items
           if (_selectedItems <3)
           {
               _warningLabel = "You must select three or more items to combo";
           }
           else
           {
               // remove last comma
               _comboString = _comboString.Substring(0, _comboString.Length - 1);
           }


           string _upsellPrice = upsellAmount.ToString();

           List<string[]> _parameters = new List<string[]>()
            {
                new string[] { "@combo_id", randName.ToString() },
                new string[] { "@prod_name", "'" +  _comboString + "'"},
                new string[] { "@date_from", "'" + (Convert.ToDateTime(Combo_DateFrom.SelectedDate)).ToString("yyyy-MM-dd") + "'" }, // Combo_DateFrom.DbSelectedDate.ToString() + "'" },
                new string[] { "@date_to", "'" +(Convert.ToDateTime(Combo_DateTo.SelectedDate)).ToString("yyyy-MM-dd") + "'"  },
                new string[] { "@time_from", "'" +Combo_TimeFrom.SelectedTime.ToString()  + "'" },
                new string[] { "@time_to", "'" +Combo_TimeTo.SelectedTime.ToString() + "'"  },
                new string[] { "@active", Combo_Active.Checked ? "1" : "0"},
                new string[] { "@upsellPrice", _upsellPrice}

            };


           for (int i = 0; i < _parameters.Count; i++)
           {
               _insertCommand += _parameters[i][1];
               if (i == _parameters.Count - 1)
                   _insertCommand += ")";
               else 
                   _insertCommand += ",";

           }
           
           
           // Dates must be correct
           if (Combo_DateFrom.SelectedDate > Combo_DateTo.SelectedDate)
           {
               _warningLabel = "Date from cannot overlap date to";
           }
           // Time must be correct
           if (Combo_TimeFrom.SelectedTime > Combo_TimeTo.SelectedTime)
           {
               _warningLabel = "Time from cannot overlap time to";
           }

           //ComboLabel.Text = _gurr;

           if (String.IsNullOrEmpty(_warningLabel))
           {
               ComboLabel.Text =  "Combo successfully stored";
               Data_Utilities.ModifyDataBase_Parameters(_insertCommand, _parameters);

               Combo_Manage.Rebind();
           }
           else
           {
               ComboLabel.Text = _warningLabel;
           }
       }

       protected void Combo_Manage_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
       {
           RadGrid _sender = (RadGrid)sender;

           string _selectionString = "select * from [shopprojectdata].[dbo].[UPSELL]";
           DataTable _table = Data_Utilities.getSQLDataByQuery(_selectionString);

           _table.PrimaryKey = new DataColumn[] { _table.Columns["COMBO_ID"] };

           _sender.DataSource = _table;
       }

       protected void Combo_Manage_DeleteCommand(object sender, GridCommandEventArgs e)
       {
           string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["COMBO_ID"].ToString();
           
            List<string[]> _parameters = new List<string[]>()
            {
                new string[] { "@combo_id", ID },
            };
            string _deleteCommand = "delete from [shopprojectdata].[dbo].[UPSELL] where [COMBO_ID] = @combo_id";
            Data_Utilities.ModifyDataBase_Parameters(_deleteCommand, _parameters);

            ComboLabel.Text = "Combo successfully deleted";
           Combo_Manage.Rebind();
       }
       

    }
}